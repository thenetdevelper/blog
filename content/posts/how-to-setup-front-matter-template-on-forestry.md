+++
date = "2018-10-15T04:39:07+00:00"
description = "Forestry allows you to create Front-matter template for your posts. You can even create multiple Front matter. In this post you'll learn how to create Front matter template on Forestry CMS."
draft = true
featured_image = ""
slug = ""
title = "How to setup front matter template on Forestry"

+++
