---
title: I'm Jeremiah Succeed
date: 2018-09-17 05:14:48 +0000

---
A **Web developer** in Abuja with deep experience on web data and APIs. I build and design modern websites with different tools, mostly JavaScript and PHP. Just to let you know, I do create and sell website components.

I write on modern web development and the good things. If you like the contents on this blog, I encourage you to join my mailing list to get my fresh contents.

Need a website? or want to work with me. You can chat me up on <a href="t.me/ijsucceed">Telegram</a>. 

I am also on <a href="https://twitter.com/ijsucceed">twitter</a>.

Feel free to ask me any question. And if I have worked on your project in the past, you can comment on your experience with me.

<p>
<i class="fa fa-twitter"></i>

